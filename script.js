let a = prompt("Enter first number");
a = Number(a);
while (isNaN(a)) {
  a = prompt("Enter first number again");
}

let b = prompt("Enter second number");
b = Number(b);
while (isNaN(b)) {
  b = prompt("Enter second number again");
}

const fraction = (a, b) => a / b;
const summ = (a, b) => a + b;
const difference = (a, b) => a - b;
const multiply = (a, b) => a * b;

function result(a, b, fn) {
  return fn(a, b);
}

let action = prompt("Enter math operator");

switch (action) {
  case "/":
    console.log(result(a, b, fraction));
    break;
  case "+":
    console.log(result(a, b, summ));
    break;
  case "-":
    console.log(result(a, b, difference));
    break;
  case "*":
    console.log(result(a, b, multiply));
    break;
  default:
    action = alert("Invalid math operator");
}
